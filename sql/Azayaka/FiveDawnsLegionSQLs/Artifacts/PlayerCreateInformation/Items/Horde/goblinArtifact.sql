#Goblin

#Warrior
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '1', '128910', '1'); 
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '1', '128289', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '1', '128908', '1');

#Hunter
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '3', '128861', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '3', '128826', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '3', '128808', '1');

#Rogue
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '4', '128870', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '4', '128476', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '4', '128872', '1');

#Priest
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '5', '128868', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '5', '128827', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '5', '128825', '1');

#Shaman
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '7', '128935', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '7', '128819', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '7', '128911', '1');

#Mage
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '8', '128862', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '8', '127587', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '8', '128820', '1');

#Warlock
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '9', '128942', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '9', '128943', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '9', '128941', '1');

#Deathknight
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '6', '128403', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '6', '128402', '1');
INSERT INTO `world`.`playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES ('9', '6', '128292', '1');