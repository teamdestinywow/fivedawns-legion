/*
*
*	FiveDawns:Legion 7.3.5
*		NpcTeleporter.cpp
*			Author: Genius/JordanG
*				WorkInProgress
*
*/


#include "Creature.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "World.h"
 
class tele : public CreatureScript
{
public: tele() : CreatureScript("tele") {}

		bool OnGossipHello(Player* player, Creature* creature)
		{
			if (player->IsInCombat())
			{
				// player->GetSession()->SendNotification("|cffFFFF00MTG - Teleporter \n |cffFFFFFFYou are in combat!");
                CloseGossipMenuFor(player);
				return false;
			}
            std::ostringstream gossipText;
            gossipText << "Starter Mall";
            std::ostringstream gossipText2;
            gossipText2 << "Duelzone";
            std::ostringstream gossipText3;
            gossipText3 << "Gurubashi Arena";
            AddGossipItemFor(player, GOSSIP_ICON_CHAT, gossipText.str(), GOSSIP_SENDER_MAIN, 1);
            AddGossipItemFor(player, GOSSIP_ICON_CHAT, gossipText2.str(), GOSSIP_SENDER_MAIN, 2);
            AddGossipItemFor(player, GOSSIP_ICON_CHAT, gossipText3.str(), GOSSIP_SENDER_MAIN, 3);
			

            SendGossipMenuFor(player, player->GetGossipTextId(creature), creature->GetGUID());
			return true;
		}

		bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 action)
		{
            CloseGossipMenuFor(player);
			switch (action)
			{
				case 1:
					player->TeleportTo(1220, -835.227f, 4277.15f, 746.252f, 1.016f); // MAP, X, Y, Z, O
					break;
				case 2:
					player->TeleportTo(1220, 337.256f, 3191.195f, 145.231f, 0.794f);
					break;
				case 3:
                    player->TeleportTo(0, -13228.622070f, 227.733826f, 32.845123f, 1.106999f);
					break;
			}
			return true;
		}
	};

void AddSC_Custom_Teleporter()
{
        new tele();
}
