#include "ScriptPCH.h"
#include "Chat.h"
#include "ScriptMgr.h"
#include "TemplateNPC.h"
#include "Creature.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "World.h"
#include "DatabaseEnv.h"
#include "Log.h"
#include "Item.h"
#include "Unit.h"
#include "WorldSession.h"
void sTemplateNPC::EquipTemplateGear(Player* player) {
    if (player->getRace() == RACE_HUMAN) {
        for (HumanGearContainer::const_iterator itr = m_HumanGearContainer.begin(); itr != m_HumanGearContainer.end(); ++itr) {
            if ((*itr)->playerClass == GetClassString(player).c_str() && (*itr)->playerSpec == sTalentsSpec) {
                if ((*itr)->pos == 15)
                    player->AutoUnequip((*itr)->pos);
                player->EquipNewItem((*itr)->pos, (*itr)->itemEntry, true); // Equip the item and apply enchants and gems
            }
        }
    } else if (player->GetTeam() == ALLIANCE && player->getRace() != RACE_HUMAN) {
        for (AllianceGearContainer::const_iterator itr = m_AllianceGearContainer.begin(); itr != m_AllianceGearContainer.end(); ++itr) {
            if ((*itr)->playerClass == GetClassString(player).c_str() && (*itr)->playerSpec == sTalentsSpec) {
                if ((*itr)->pos == 15)
                    player->AutoUnequip((*itr)->pos);
                player->EquipNewItem((*itr)->pos, (*itr)->itemEntry, true); // Equip the item and apply enchants and gems
            }
        }
    } else if (player->GetTeam() == HORDE) {
        for (HordeGearContainer::const_iterator itr = m_HordeGearContainer.begin(); itr != m_HordeGearContainer.end(); ++itr) {
            if ((*itr)->playerClass == GetClassString(player).c_str() && (*itr)->playerSpec == sTalentsSpec) {
                if ((*itr)->pos == 15)
                    player->AutoUnequip((*itr)->pos);
                player->EquipNewItem((*itr)->pos, (*itr)->itemEntry, true); // Equip the item and apply enchants and gems
            }
        }
    }
}

void sTemplateNPC::LoadHumanGearContainer() {
    for (HumanGearContainer::const_iterator itr = m_HumanGearContainer.begin(); itr != m_HumanGearContainer.end(); ++itr)
        delete *itr;

    m_HumanGearContainer.clear();

	QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec, pos, itemEntry FROM template_npc_human;");

    uint32 oldMSTime = getMSTime();
    uint32 count = 0;

    if (!result) {
		TC_LOG_INFO("server.loading", ">> Loaded 0 'gear templates. DB table `template_npc_human` is empty!");
        return;
    }

    do {
        Field* fields = result->Fetch();

        HumanGearTemplate* pItem = new HumanGearTemplate;

        pItem->playerClass = fields[0].GetString();
        pItem->playerSpec = fields[1].GetString();
        pItem->pos = fields[2].GetUInt8();
        pItem->itemEntry = fields[3].GetUInt32();

        m_HumanGearContainer.push_back(pItem);
        ++count;
    } while (result->NextRow());
	TC_LOG_INFO("server.loading", ">> Loaded %u gear templates for Humans in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

void sTemplateNPC::LoadAllianceGearContainer() {
    for (AllianceGearContainer::const_iterator itr = m_AllianceGearContainer.begin(); itr != m_AllianceGearContainer.end(); ++itr)
        delete *itr;

    m_AllianceGearContainer.clear();

	QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec, pos, itemEntry FROM template_npc_alliance;");

    uint32 oldMSTime = getMSTime();
    uint32 count = 0;

    if (!result) {
		TC_LOG_INFO("server.loading", ">> Loaded 0 'gear templates. DB table `template_npc_alliance` is empty!");
        return;
    }

    do {
        Field* fields = result->Fetch();

        AllianceGearTemplate* pItem = new AllianceGearTemplate;

        pItem->playerClass = fields[0].GetString();
        pItem->playerSpec = fields[1].GetString();
        pItem->pos = fields[2].GetUInt8();
        pItem->itemEntry = fields[3].GetUInt32();

        m_AllianceGearContainer.push_back(pItem);
        ++count;
    } while (result->NextRow());
	TC_LOG_INFO("server.loading", ">> Loaded %u gear templates for Alliances in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

void sTemplateNPC::LoadHordeGearContainer() {
    for (HordeGearContainer::const_iterator itr = m_HordeGearContainer.begin(); itr != m_HordeGearContainer.end(); ++itr)
        delete *itr;

    m_HordeGearContainer.clear();

    QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec, pos, itemEntry FROM template_npc_horde;");

    uint32 oldMSTime = getMSTime();
    uint32 count = 0;

    if (!result) {
		TC_LOG_INFO("server.loading", ">> Loaded 0 'gear templates. DB table `template_npc_horde` is empty!");
        return;
    }

    do {
        Field* fields = result->Fetch();

        HordeGearTemplate* pItem = new HordeGearTemplate;

        pItem->playerClass = fields[0].GetString();
        pItem->playerSpec = fields[1].GetString();
        pItem->pos = fields[2].GetUInt8();
        pItem->itemEntry = fields[3].GetUInt32();

        m_HordeGearContainer.push_back(pItem);
        ++count;
    } while (result->NextRow());
	TC_LOG_INFO("server.loading", ">> Loaded %u gear templates for Hordes in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

std::string sTemplateNPC::GetClassString(Player* player) {
    switch (player->getClass()) {
    case CLASS_PRIEST:
        return "Priest";
        break;
    case CLASS_PALADIN:
        return "Paladin";
        break;
    case CLASS_WARRIOR:
        return "Warrior";
        break;
    case CLASS_MAGE:
        return "Mage";
        break;
    case CLASS_WARLOCK:
        return "Warlock";
        break;
    case CLASS_SHAMAN:
        return "Shaman";
        break;
    case CLASS_DRUID:
        return "Druid";
        break;
    case CLASS_HUNTER:
        return "Hunter";
        break;
    case CLASS_ROGUE:
        return "Rogue";
        break;
    case CLASS_DEATH_KNIGHT:
        return "DeathKnight";
        break;
    case CLASS_MONK:
        return "Monk";
        break;
    case CLASS_DEMON_HUNTER:
        return "DemonHunter";
        break;
    default:
        break;
    }
    return "Unknown"; // Fix warning, this should never happen
}

bool sTemplateNPC::OverwriteTemplate(Player* player, std::string& playerSpecStr) {
    // Delete old gear templates before extracting new ones
    if (player->getRace() == RACE_HUMAN) {
        CharacterDatabase.PExecute("DELETE FROM template_npc_human WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());
        player->GetSession()->SendNotification("Template successfuly created!");
        return false;
    } else if (player->GetTeam() == ALLIANCE && player->getRace() != RACE_HUMAN) {
        CharacterDatabase.PExecute("DELETE FROM template_npc_alliance WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());
        player->GetSession()->SendNotification("Template successfuly created!");
        return false;
    } else if (player->GetTeam() == HORDE) {
        // ????????????? sTemplateNpcMgr here??
        CharacterDatabase.PExecute("DELETE FROM template_npc_horde WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());
        player->GetSession()->SendNotification("Template successfuly created!");
        return false;
    }
    return true;
}

void sTemplateNPC::ExtractGearTemplateToDB(Player* player, std::string& playerSpecStr) 
{
    for (uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i) {
        Item* equippedItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i);

        if (equippedItem) {
            if (player->getRace() == RACE_HUMAN) {
				CharacterDatabase.PExecute("INSERT INTO template_npc_human (`playerClass`, `playerSpec`, `pos`, `itemEntry`) VALUES ('%s', '%s', '%u', '%u');"
                                           , GetClassString(player).c_str(), playerSpecStr.c_str(), equippedItem->GetSlot(), equippedItem->GetEntry()/*, equippedItem->GetEnchantmentId(REFORGE_ENCHANTMENT_SLOT)*/);
            } else if (player->GetTeam() == ALLIANCE && player->getRace() != RACE_HUMAN) {
				CharacterDatabase.PExecute("INSERT INTO template_npc_alliance (`playerClass`, `playerSpec`, `pos`, `itemEntry`) VALUES ('%s', '%s', '%u', '%u');"
                                           , GetClassString(player).c_str(), playerSpecStr.c_str(), equippedItem->GetSlot(), equippedItem->GetEntry()/*, equippedItem->GetEnchantmentId(REFORGE_ENCHANTMENT_SLOT)*/);
            } else if (player->GetTeam() == HORDE) {
                CharacterDatabase.PExecute("INSERT INTO template_npc_horde (`playerClass`, `playerSpec`, `pos`, `itemEntry`) VALUES ('%s', '%s', '%u', '%u');"
                    , GetClassString(player).c_str(), playerSpecStr.c_str(), equippedItem->GetSlot(), equippedItem->GetEntry() /*equippedItem->GetEnchantmentId(REFORGE_ENCHANTMENT_SLOT)*/);
 			}
            }
        }
}

bool sTemplateNPC::CanEquipTemplate(Player* player, std::string& playerSpecStr) {
    if (player->getRace() == RACE_HUMAN) {
        QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec FROM template_npc_human WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());
        if (!result)
            return false;
    } else if (player->GetTeam() == ALLIANCE && player->getRace() != RACE_HUMAN) {
        QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec FROM template_npc_alliance "
                             "WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());

        if (!result)
            return false;
    } else if (player->GetTeam() == HORDE) {
        QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec FROM template_npc_horde "
                             "WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());

        if (!result)
            return false;
    }
    return true;
}

enum Specializations{
    PRIEST_DISCIPLINE,
    PRIEST_HOLY,
    PRIEST_SHADOW,
    PALADIN_HOLY,
    PALADIN_PROTECTION,
    PALADIN_RETRIBUTION,
    WARRIOR_ARMS,
    WARRIOR_FURY,
    WARRIOR_PROTECTION,
    MAGE_ARCANE,
    MAGE_FIRE,
    MAGE_FROST,
    WARLOCK_AFFLICTION,
    WARLOCK_DEMONOLOGY,
    WARLOCK_DESTRUCTION,
    SHAMAN_ELEMENTAL,
    SHAMAN_ENHANCEMENT,
    SHAMAN_RESTORATION,
    DRUID_BALANCE,
    DRUID_FERAL,
    DRUID_RESTORATION,
    HUNTER_MARKSMANSHIP,
    HUNTER_BEASTMASTERY,
    HUNTER_SURVIVAL,
    ROGUE_ASSASSINATION,
    ROGUE_COMBAT,
    ROGUE_SUBTLETY,
    MONK_WINDWALKER,
    MONK_MISTWEAVER,
    MONK_BREWMASTER,
    DK_BLOOD,
    DK_FROST,
    DK_UNHOLY,
    DH_HAVOC,
    DH_VENGEANCE,
    TEMPLATE_INSERT
};

// Creature Script * TemplateNPC *
class TemplateNPC : public CreatureScript {
  public:
    TemplateNPC() : CreatureScript("TemplateNPC") { }

    bool OnGossipHello(Player* player, Creature* creature) {
        Player* _player2 = player;
        switch (player->getClass()) {
        case CLASS_PRIEST: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_wordfortitude:26:26:-13:1|t|r Use Discipline ", GOSSIP_SENDER_MAIN, PRIEST_DISCIPLINE);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_holybolt:26:26:-13:1|t|r Use Holy ",GOSSIP_SENDER_MAIN, PRIEST_HOLY);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_shadow_shadowwordpain:26:26:-13:1|t|r Use Shadow ", GOSSIP_SENDER_MAIN, PRIEST_SHADOW);
        }
        break;
        case CLASS_PALADIN: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_holybolt:26:26:-13:1|t|r Use Holy ", GOSSIP_SENDER_MAIN, PALADIN_HOLY);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_auraoflight:26:26:-13:1|t|r Use Retribution ", GOSSIP_SENDER_MAIN, PALADIN_RETRIBUTION);
        }
        break;
        case CLASS_WARRIOR: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_warrior_innerrage:26:26:-13:1|t|r Use Fury ", GOSSIP_SENDER_MAIN, WARRIOR_FURY);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_warrior_savageblow:26:26:-13:1|t|r Use Arms ",GOSSIP_SENDER_MAIN, WARRIOR_ARMS);
        }
        break;
        case CLASS_MAGE: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_magicalsentry:26:26:-13:1|t|r Use Arcane ",GOSSIP_SENDER_MAIN, MAGE_ARCANE);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_fire_flamebolt:26:26:-13:1|t|r Use Fire ",GOSSIP_SENDER_MAIN, MAGE_FIRE);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_frost_frostbolt02:26:26:-13:1|t|r Use Frost ",GOSSIP_SENDER_MAIN, MAGE_FROST);
        }
        break;
        case CLASS_WARLOCK: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_shadow_deathcoil:26:26:-13:1|t|r Use Affliction ",GOSSIP_SENDER_MAIN, WARLOCK_AFFLICTION);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_shadow_metamorphosis:26:26:-13:1|t|r Use Demonology ",GOSSIP_SENDER_MAIN, WARLOCK_DEMONOLOGY);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_shadow_rainoffire:26:26:-13:1|t|r Use Destruction ",GOSSIP_SENDER_MAIN, WARLOCK_DESTRUCTION);
        }
        break;
        case CLASS_SHAMAN: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_lightning:26:26:-13:1|t|r Use Elemental ",GOSSIP_SENDER_MAIN, SHAMAN_ELEMENTAL);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_lightningshield:26:26:-13:1|t|r Use Enhancement ",GOSSIP_SENDER_MAIN, SHAMAN_ENHANCEMENT);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_magicimmunity:26:26:-13:1|t|r Use Restoration ",GOSSIP_SENDER_MAIN, SHAMAN_RESTORATION);
        }
        break;
        case CLASS_DRUID: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_starfall:26:26:-13:1|t|r Use Balance ",GOSSIP_SENDER_MAIN, DRUID_BALANCE);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_racial_bearform:26:26:-13:1|t|r Use Feral ",GOSSIP_SENDER_MAIN, DRUID_FERAL);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_healingtouch:26:26:-13:1|t|r Use Restoration ",GOSSIP_SENDER_MAIN, DRUID_RESTORATION);
        }
        break;
        case CLASS_HUNTER: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_hunter_focusedaim:26:26:-13:1|t|r Use Markmanship ",GOSSIP_SENDER_MAIN, HUNTER_MARKSMANSHIP);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_hunter_bestialdiscipline:26:26:-13:1|t|r Use Beastmastery ",GOSSIP_SENDER_MAIN, HUNTER_BEASTMASTERY);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_hunter_camouflage:26:26:-13:1|t|r Use Survival ",GOSSIP_SENDER_MAIN, HUNTER_SURVIVAL);
        }
        break;
        case CLASS_ROGUE: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_rogue_eviscerate:26:26:-13:1|t|r Use Assasination ",GOSSIP_SENDER_MAIN, ROGUE_ASSASSINATION);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_backstab:26:26:-13:1|t|r Use Combat ",GOSSIP_SENDER_MAIN, ROGUE_COMBAT);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_stealth:26:26:-13:1|t|r Use Subtlety ",GOSSIP_SENDER_MAIN, ROGUE_SUBTLETY);
        }
        break;
        case CLASS_DEATH_KNIGHT: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_bloodpresence:26:26:-13:1|t|r Use Blood ",GOSSIP_SENDER_MAIN, DK_BLOOD);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_frostpresence:26:26:-13:1|t|r Use Frost ",GOSSIP_SENDER_MAIN, DK_FROST);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_unholypresence:36:36:-13:1|t|r Use Unholy ",GOSSIP_SENDER_MAIN, DK_UNHOLY);
        }
        break;
        case CLASS_DEMON_HUNTER: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_bloodpresence:26:26:-13:1|t|r Use Havoc ", GOSSIP_SENDER_MAIN, DH_HAVOC);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_frostpresence:26:26:-13:1|t|r Use Vengeance ", GOSSIP_SENDER_MAIN, DH_VENGEANCE);
        }
        break;
        case CLASS_MONK: {
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_bloodpresence:26:26:-13:1|t|r Use Windwalker ", GOSSIP_SENDER_MAIN, MONK_WINDWALKER);
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_frostpresence:26:26:-13:1|t|r Use Mistweaver ", GOSSIP_SENDER_MAIN, MONK_MISTWEAVER);
        }
        break;
        }
        if (player->GetSession()->GetSecurity() >= 3)
            AddGossipItemFor(player, GOSSIP_ICON_TABARD, "Game Master: Insert Template", GOSSIP_SENDER_MAIN, TEMPLATE_INSERT);

        SendGossipMenuFor(player, player->GetGossipTextId(creature), creature->GetGUID());
        return true;
    }

	// Merge
    void EquipFullTemplateGear(Player* player, std::string& playerSpecStr) 
	{
        if (sTemplateNpcMgr->CanEquipTemplate(player, playerSpecStr) == false) {
            return;
        }
        sTemplateNpcMgr->EquipTemplateGear(player);
    }

    void InsertTemplate(Player* player)
    {
        switch (player->GetUInt32Value(PLAYER_FIELD_CURRENT_SPEC_ID))
        {
            case TALENT_SPEC_DEATHKNIGHT_BLOOD:
                sTemplateNpcMgr->sTalentsSpec = "Blood";
                break;
            case TALENT_SPEC_DEATHKNIGHT_FROST:
                sTemplateNpcMgr->sTalentsSpec = "Frost";
                break;
            case TALENT_SPEC_DEATHKNIGHT_UNHOLY:
                sTemplateNpcMgr->sTalentsSpec = "Unholy";
                break;
            case TALENT_SPEC_PALADIN_PROTECTION:
                sTemplateNpcMgr->sTalentsSpec = "Protection";
                break;
            case TALENT_SPEC_PALADIN_RETRIBUTION:
                sTemplateNpcMgr->sTalentsSpec = "Retribution";
                break;
            case TALENT_SPEC_WARRIOR_ARMS:
                sTemplateNpcMgr->sTalentsSpec = "Arms";
                break;
            case TALENT_SPEC_WARRIOR_FURY:
                sTemplateNpcMgr->sTalentsSpec = "Fury";
                break;
            case TALENT_SPEC_WARRIOR_PROTECTION:
                sTemplateNpcMgr->sTalentsSpec = "Protection";
                break;
            case TALENT_SPEC_MONK_BREWMASTER:
                sTemplateNpcMgr->sTalentsSpec = "Brewmaster";
                break;
            case TALENT_SPEC_MAGE_ARCANE:
                sTemplateNpcMgr->sTalentsSpec = "Arcane";
                break;
            case TALENT_SPEC_MAGE_FIRE:
                sTemplateNpcMgr->sTalentsSpec = "Fire";
                break;
            case TALENT_SPEC_MAGE_FROST:
                sTemplateNpcMgr->sTalentsSpec = "Frost";
                break;
            case TALENT_SPEC_PALADIN_HOLY:
                sTemplateNpcMgr->sTalentsSpec = "Holy";
                break;
            case TALENT_SPEC_DRUID_BALANCE:
                sTemplateNpcMgr->sTalentsSpec = "Balance";
                break;
            case TALENT_SPEC_PRIEST_DISCIPLINE:
                sTemplateNpcMgr->sTalentsSpec = "Discipline";
                break;
            case TALENT_SPEC_PRIEST_HOLY:
                sTemplateNpcMgr->sTalentsSpec = "Holy";
                break;
            case TALENT_SPEC_PRIEST_SHADOW:
                sTemplateNpcMgr->sTalentsSpec = "Shadow";
                break;
            case TALENT_SPEC_DRUID_RESTORATION:
                sTemplateNpcMgr->sTalentsSpec = "Restoration";
                break;
            case TALENT_SPEC_SHAMAN_ELEMENTAL:
                sTemplateNpcMgr->sTalentsSpec = "Elemental";
                break;
            case TALENT_SPEC_SHAMAN_RESTORATION:
                sTemplateNpcMgr->sTalentsSpec = "Restoration";
                break;
            case TALENT_SPEC_WARLOCK_AFFLICTION:
                sTemplateNpcMgr->sTalentsSpec = "Affliction";
                break;
            case TALENT_SPEC_WARLOCK_DEMONOLOGY:
                sTemplateNpcMgr->sTalentsSpec = "Demonology";
                break;
            case TALENT_SPEC_WARLOCK_DESTRUCTION:
                sTemplateNpcMgr->sTalentsSpec = "Destruction";
                break;
            case TALENT_SPEC_MONK_MISTWEAVER:
                sTemplateNpcMgr->sTalentsSpec = "Mistweaver";
                break;
            case TALENT_SPEC_DRUID_CAT:
                sTemplateNpcMgr->sTalentsSpec = "Feral";
                break;
            case TALENT_SPEC_DRUID_BEAR:
                sTemplateNpcMgr->sTalentsSpec = "Guardian";
                break;
            case TALENT_SPEC_HUNTER_BEASTMASTER:
                sTemplateNpcMgr->sTalentsSpec = "Beastmastery";
                break;
            case TALENT_SPEC_HUNTER_MARKSMAN:
                sTemplateNpcMgr->sTalentsSpec = "Marksmanship";
                break;
            case TALENT_SPEC_HUNTER_SURVIVAL:
                sTemplateNpcMgr->sTalentsSpec = "Survival";
                break;
            case TALENT_SPEC_ROGUE_ASSASSINATION:
                sTemplateNpcMgr->sTalentsSpec = "Assassination";
                break;
            case TALENT_SPEC_ROGUE_COMBAT:
                sTemplateNpcMgr->sTalentsSpec = "Outlaw";
                break;
            case TALENT_SPEC_ROGUE_SUBTLETY:
                sTemplateNpcMgr->sTalentsSpec = "Subtlety";
                break;
            case TALENT_SPEC_SHAMAN_ENHANCEMENT:
                sTemplateNpcMgr->sTalentsSpec = "Enhancement";
                break;
            case TALENT_SPEC_DEMON_HUNTER_HAVOC:
                sTemplateNpcMgr->sTalentsSpec = "Havoc";
                break;
            case TALENT_SPEC_DEMON_HUNTER_VENGEANCE:
                sTemplateNpcMgr->sTalentsSpec = "Vengeance";
                break;
            case TALENT_SPEC_MONK_BATTLEDANCER:
                sTemplateNpcMgr->sTalentsSpec = "Windwalker";
                break;
        }
        
        sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
        sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
    }
    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 action)
    {
        sTemplateNpcMgr->LoadHumanGearContainer();
        sTemplateNpcMgr->LoadAllianceGearContainer();
        sTemplateNpcMgr->LoadHordeGearContainer();
        CloseGossipMenuFor(player);
        switch (action)
        {
        case MAGE_FROST:
            sTemplateNpcMgr->sTalentsSpec = "Frost";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case MAGE_FIRE:
            sTemplateNpcMgr->sTalentsSpec = "Fire";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case MAGE_ARCANE:
            sTemplateNpcMgr->sTalentsSpec = "Arcane";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case DRUID_FERAL:
            sTemplateNpcMgr->sTalentsSpec = "Feral";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case DRUID_BALANCE:
            sTemplateNpcMgr->sTalentsSpec = "Balance";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case DRUID_RESTORATION:
            sTemplateNpcMgr->sTalentsSpec = "Restoration";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case HUNTER_MARKSMANSHIP:
            sTemplateNpcMgr->sTalentsSpec = "Marksmanship";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case HUNTER_BEASTMASTERY:
            sTemplateNpcMgr->sTalentsSpec = "Beastmastery";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case HUNTER_SURVIVAL:
            sTemplateNpcMgr->sTalentsSpec = "Survival";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case PRIEST_HOLY:
            sTemplateNpcMgr->sTalentsSpec = "Holy";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case PRIEST_DISCIPLINE:
            sTemplateNpcMgr->sTalentsSpec = "Discipline";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case PRIEST_SHADOW:
            sTemplateNpcMgr->sTalentsSpec = "Shadow";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case WARLOCK_AFFLICTION:
            sTemplateNpcMgr->sTalentsSpec = "Affliction";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case WARLOCK_DESTRUCTION:
            sTemplateNpcMgr->sTalentsSpec = "Destruction";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case WARLOCK_DEMONOLOGY:
            sTemplateNpcMgr->sTalentsSpec = "Demonology";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case PALADIN_PROTECTION:
            sTemplateNpcMgr->sTalentsSpec = "Protection";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case PALADIN_RETRIBUTION:
            sTemplateNpcMgr->sTalentsSpec = "Retribution";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case PALADIN_HOLY:
            sTemplateNpcMgr->sTalentsSpec = "Holy";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case WARRIOR_ARMS:
            sTemplateNpcMgr->sTalentsSpec = "Arms";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case WARRIOR_FURY:
            sTemplateNpcMgr->sTalentsSpec = "Fury";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case SHAMAN_ELEMENTAL:
            sTemplateNpcMgr->sTalentsSpec = "Elemental";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case SHAMAN_ENHANCEMENT:
            sTemplateNpcMgr->sTalentsSpec = "Enhancement";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case SHAMAN_RESTORATION:
            sTemplateNpcMgr->sTalentsSpec = "Restoration";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case ROGUE_ASSASSINATION:
            sTemplateNpcMgr->sTalentsSpec = "Assassination";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case ROGUE_COMBAT:
            sTemplateNpcMgr->sTalentsSpec = "Outlaw";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case ROGUE_SUBTLETY:
            sTemplateNpcMgr->sTalentsSpec = "Subtlety";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case DK_BLOOD:
            sTemplateNpcMgr->sTalentsSpec = "Blood";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case DK_FROST:
            sTemplateNpcMgr->sTalentsSpec = "Frost";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case DK_UNHOLY:
            sTemplateNpcMgr->sTalentsSpec = "Unholy";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case DH_HAVOC:
            sTemplateNpcMgr->sTalentsSpec = "Havoc";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case DH_VENGEANCE:
            sTemplateNpcMgr->sTalentsSpec = "Vengeance";
            EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
            player->GetSession()->SendNotification("Spec successfully equipped!");
            break;
        case TEMPLATE_INSERT:
            InsertTemplate(player);
            break;
        }
        return true;
    }
};

void AddSC_Custom_TemplateNPC() {
    new TemplateNPC();
}
