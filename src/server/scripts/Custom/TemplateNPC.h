#ifndef TALENT_FUNCTIONS_H
#define TALENT_FUNCTIONS_H

#include "Define.h"

enum NoGossipIcon
{
	NO_GOSSIP_ICON = -1, // Only Works on 4.3.4
};

struct HumanGearTemplate
{
	std::string    playerClass;
	std::string    playerSpec;
	uint8          pos;
	uint32         itemEntry;
};

struct AllianceGearTemplate
{
	std::string    playerClass;
	std::string    playerSpec;
	uint8          pos;
	uint32         itemEntry;
};

struct HordeGearTemplate
{
	std::string    playerClass;
	std::string    playerSpec;
	uint8          pos;
	uint32         itemEntry;
};

typedef std::vector<HumanGearTemplate*> HumanGearContainer;
typedef std::vector<AllianceGearTemplate*> AllianceGearContainer;
typedef std::vector<HordeGearTemplate*> HordeGearContainer;

class sTemplateNPC
{
public:
	static sTemplateNPC* instance()
	{
		static sTemplateNPC* instance = new sTemplateNPC();
		return instance;
	}

	void LoadHumanGearContainer();
	void LoadAllianceGearContainer();
	void LoadHordeGearContainer();

	bool OverwriteTemplate(Player* /*player*/, std::string& /*playerSpecStr*/);
	void ExtractGearTemplateToDB(Player* /*player*/, std::string& /*playerSpecStr*/);
	bool CanEquipTemplate(Player* /*player*/, std::string& /*playerSpecStr*/);

	std::string GetClassString(Player* /*player*/);
	std::string sTalentsSpec;
	
	void EquipTemplateGear(Player* /*player*/);

	HumanGearContainer m_HumanGearContainer;
	AllianceGearContainer m_AllianceGearContainer;
	HordeGearContainer m_HordeGearContainer;
};
#define sTemplateNpcMgr sTemplateNPC::instance()
#endif